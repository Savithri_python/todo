# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Register(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
	dob = models.DateField()
	phone_number = models.IntegerField()
	address = models.TextField()
	interests = models.TextField()

	def __str__(self):
		return self.user.username

