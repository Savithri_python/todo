from rest_framework.serializers import ModelSerializer

from django.contrib.auth.models import User
from .models import Register


class RegisterSerializer(ModelSerializer):

    class Meta:
        model = Register
        fields = ('dob', 'phone_number', 'address', 'interests')


class UserSerializer(ModelSerializer):
    profile = RegisterSerializer(many=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'first_name',
            'last_name', 'profile')

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        user_obj = User.objects.create(**validated_data)
        Register.objects.create(user=user_obj, **profile_data)
        return user_obj
        
