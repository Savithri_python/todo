from django.conf.urls import url, include
from rest_framework import routers
from .views import RegisterView, LoginAPI

# router = routers.DefaultRouter()
# router.register(r'register', RegisterView)

urlpatterns = [
	url(r'^$', RegisterView.as_view(), name='register'),
	url(r'^login/$', LoginAPI.as_view(), name='login'),
]