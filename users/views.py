# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework import generics
from .serializers import UserSerializer
from .models import Register

# Create your views here.

class RegisterView(CreateAPIView):
	serializer_class = UserSerializer
	queryset = User.objects.all()

# class ProfileUpdateView(generics.RetrieveUpdateDestroyAPIView):
# 	serializer_class = 
# 	queryset = Register.objects.all()
# 	authentication_classes = (BasicAuthentication, SessionAuthentication)
# 	permission_classes = (IsAuthenticated, )

class LoginAPI(ListAPIView):
	authentication_classes = (BasicAuthentication, SessionAuthentication)
	permission_classes = (IsAuthenticated, )
	
	def get(self, request, format=None):
		content = {
			'user': unicode(request.user),  # `django.contrib.auth.User` instance.
            'auth': unicode(request.auth),
		}
		return Response(content)